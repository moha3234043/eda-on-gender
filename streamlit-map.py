import streamlit as st
import pandas as pd
import plotly.express as px


# Use the `st.markdown` function to apply inline CSS
st.markdown(
    """
    <style>
    [data-testid="stAppViewContainer"]{
        background-color:#232D3F;
        text: #ffffff;

    }
    [id="eda-on-gender"]{
        color: #ffffff;
    }
    [data-testid="stMarkdownContainer"] {
        color: #FFFFFF; /* Set text color to white */
    }
    [data-testid="StyledLinkIconContainer"]{
        color: #FFFFFF; /* Set text color to white */

    }
    [id="interactive-map"]{
        color: #FFFFFF; /* Set text color to white */
    }

    </style>
    """,
    unsafe_allow_html=True
)

# Load your DataFrame (replace 'your_data.csv' with your actual data file)
df = pd.read_csv('./datasets/kiva_loans_clean.csv')

st.title("EDA on Gender")

# Add a title for the map section
st.header("Interactive Map")

# Rename the 'funded_amount' column to 'Funded amount'
df.rename(columns={'funded_amount': 'Funded Amount'}, inplace=True)

# Rename the 'activity' column to 'Activity'
df.rename(columns={'activity': 'Activity'}, inplace=True)

# Rename the 'repayment_interval' column to 'Repayment Interval'
df.rename(columns={'repayment_interval': 'Repayment Interval'}, inplace=True)

# Allow the user to choose a categorical feature
selected_feature = st.selectbox("Select a Categorical Feature", ['Repayment Interval', 'Activity', 'Funded Amount'])

# # Calculate the range for the 'funded_amount' column
# min_funded_amount = df['funded_amount'].min()
# max_funded_amount = df['funded_amount'].max()

# Group and aggregate data based on the selected feature
if selected_feature == 'Repayment Interval':
    grouped_data = df.groupby(['country', 'borrower_genders', 'Repayment Interval']).size().reset_index(name='count')
elif selected_feature == 'Activity':
    grouped_data = df.groupby(['country', 'borrower_genders', 'Activity']).size().reset_index(name='count')
else:  # selected_feature == 'Funded Amount'
    grouped_data = df.groupby(['country', 'borrower_genders']).agg({'Funded Amount': 'sum'}).reset_index()

# Define custom colors for specific categories
custom_colors = {
    'bullet': 'blue',
    'monthly': 'green',
    'weekly': 'orange',
    'irregular': 'red',
}


# Create an interactive map using Plotly within Streamlit
fig = px.scatter_geo(
    grouped_data,
    locations="country",
    locationmode='country names',
    color=selected_feature,
    color_discrete_map=custom_colors,
    color_continuous_scale="speed",  # Use a green gradient color scale (Viridis)
    size="count" if selected_feature in ['Repayment Interval', 'Activity'] else 'Funded Amount',
    animation_frame="borrower_genders",
    projection="natural earth",
    # range_color=(min_funded_amount, max_funded_amount)  # Set the color scale range
)

# Customize the map size
fig.update_geos(
    showcoastlines=True,
    coastlinecolor="Black",
    showcountries=True,  # Display country borders
    center={'lon': 0, 'lat': 0},  # Center of the map
    projection_scale=1.5,  # Adjust the projection scale for zoom
)

# Remove the border around the map
fig.update_layout(
    geo=dict(
        showframe=False,
    ),
    width=850,  # Adjust the width of the map
    height=800,   # Adjust the height of the map
)

# Display the map in Streamlit
st.plotly_chart(fig, color='funded_amount')
